## Next JS POC 

- Ability to proxy requests to external services via the Routes API
- The ability to separate the public and private parts of the application
- The ability to log in with Google
- Ability to generate HTML pages on the server (SSR)
