import { useSession, getSession, signIn } from 'next-auth/client'
import { useState, useEffect } from 'react'
import Layout from '../components/layout'

export default function Page () {
  const [ session, loading ] = useSession()
  const [ content , setContent ] = useState()
  

  useEffect(()=>{
    const fetchData = async () => {
      const res = await fetch('/api/examples/get-session-example')
      const json = await res.json()
      if (json.content) { setContent(json.content) }
    }
    fetchData()
  },[session])


  return (
    <Layout>
      <h1>Server Side Rendering</h1>    
      <p>Here is an example of using <strong>getSession()</strong>.</p>
      <p>
        {session && <>
          Get user name: <strong> {content} </strong>
        </>}
        {!session && <>
          <a
            href={`/api/auth/signin`}
            onClick={(e) => {
              e.preventDefault()
              signIn()
            }}
          > {content} 
          </a>
        </>}
      </p>
       
    </Layout>
  )
}

export async function getServerSideProps(context) {
  return {
    props: {
      session: await getSession(context)
    }
  }
}
