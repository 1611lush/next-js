import { getSession } from 'next-auth/client'

export default async (req, res) => {
  const session = await getSession({ req })
  if (session) {
    res.send({content:  JSON.stringify(session.user.name, null, 2)})
  } else {
    res.send({content:  'You must be signed in to see example response'})
  }
  res.end()
}