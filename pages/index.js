import Layout from '../components/layout'
import Link from 'next/link'
import { signIn, signOut, useSession } from 'next-auth/client'


export default function Page () {
  const [ session ] = useSession()

  return (
    <Layout>
	    <h1>POC of Next JS</h1>
	    <h2>Homework:</h2>
	    <ol>
		    <li>
		  	    Ability to proxy requests to external services via <Link href="/api-example"><a>the Routes API</a></Link>
		  	</li>
		    <li>
		    	The ability to separate <Link href="/"><a>the public</a></Link> and <Link href="/protected"><a>private</a></Link> parts of the application
		    </li>
		    <li>
		    	{!session && <>	          
		            <a
		                href={`/api/auth/signin`}
		                onClick={(e) => {
		                  e.preventDefault()
		                  signIn()
		                }}
		              >	               
	    				The ability to log in with Google
		              </a>
		              <span > - You are not logged in, follow the link to log in</span>
		          </>}

		        {session && <>            
	              <span>The ability to log in with Google - You are already logged in Google, </span>
	              <a
	                href={`/api/auth/signout`}	               
	                onClick={(e) => {
	                  e.preventDefault()
	                  signOut()
	                }}
	              >
                	log out
              </a>
	              
	          </>}

		    </li>
		    <li>
		    	<Link href="/server"><a>Ability to generate HTML pages on the server (SSR)</a></Link>
		    </li>
		</ol>
    </Layout>
  )
}