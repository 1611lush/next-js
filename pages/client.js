import Layout from '../components/layout'

export default function Page () {
  return (
    <Layout>
      <h1>Client Side Rendering</h1>
      <p>
        Example page of Client Side Rendering.
      </p>  
      <p>
        This page uses the <strong>useSession()</strong> React Hook in the <strong>&lt;Header/&gt;</strong> component.
      </p>      
    </Layout>
  )
}
