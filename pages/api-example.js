import Layout from '../components/layout'
import { useState, useEffect } from 'react'

export default function Page () {
  const [ dateFact, setDateFact ] = useState()

  useEffect(()=>{
    const fetchData = async () => {
      const Data = new Date();
      const month = Data.getMonth()
      const date = Data.getDate()
      const res = await fetch(`http://numbersapi.com/${month}/${date}/date?json`)
      const json = await res.json()
      if (json.text) { setDateFact(json.text) }
    }
    fetchData()
  },[])


  return (
    <Layout>
      <h1>API Example: get today in history</h1> 
      <p className="fact_info"> {dateFact} </p>       
      <img className="fact_image" src="https://picsum.photos/650/350?random=2" />      
    </Layout>
  )
}